"use strict";

var os = require("os"),
    fs = require("fs");


function getConfigLocation(withFileName){
    if(withFileName){
        return os.homedir() + "/.config/nobjs/nobjs_config.json";
    }
    return os.homedir() + "/.config/nobjs";
}


function getConfig() {
    return JSON.parse(fs.readFileSync(getConfigLocation(true), 'utf8'));
}

function hasHomeConfigDir() {
    try {
        fs.statSync(os.homedir() + "/.config");
        return true;
    }
    catch (err) {
        return false;
    }
}

function hasNobjsConfigDir() {
    try {
        fs.statSync(os.homedir() + "/.config/nobjs");
        return true;
    }
    catch (err) {
        return false;
    }
}



module.exports = {
    getConfigLocation: getConfigLocation,
    getConfig: getConfig,
    hasHomeConfigDir: hasHomeConfigDir,
    hasNobjsConfigDir: hasNobjsConfigDir
};
