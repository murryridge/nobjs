"use strict";


var prompt = require("prompt"),
    os = require("os"),
    fs = require("fs"),
    util = require("util"),
    admin = require("./admin"),
    nobjsBlogConfigFile = "nobjs_blog_config.json";

function mkLocalCfg(cb) {
    var nobjsConfig = {
        blogs: {}
    };
    if (!admin.hasHomeConfigDir()) {
        fs.mkdir(os.homedir() + "/.config", "775", function(err) {
            if (err) {cb(err); return;}
        });
    }
    fs.mkdir(admin.getConfigLocation(false), "775", function(err) {
        if (err) {cb(err); return;}
        fs.writeFile(admin.getConfigLocation(true), JSON.stringify(nobjsConfig), function(err) {
            if (err) {cb(err); return;}
            cb(null);
        });
    });
    

}


function mkdirs(config, cb) {
    for (var dir in config.dirs) {
        fs.mkdir(config.dirs[dir], "775", function(err) {
            cb(err);
        });
    }

}

function init() {
    var nobjsBlogConfig = {},
        schema = {
            properties: {
                work_directory: {
                    //pattern: /^[a-zA-Z\s\-]+$/,
                    message: 'Work directory must be only letters, spaces, or dashes',
                    required: true,
                    default: util.format("%s/%s", os.homedir(), "nobjs/work")
                },
                publish_directory: {
                    //pattern: /^[a-zA-Z\s\-]+$/,
                    message: 'Publish directory must be only letters, spaces, or dashes',
                    required: true,
                    default: util.format("%s/%s", os.homedir(), "nobjs/publish")
                },
                blog_name: {
                    message: "What is the title of the blog",
                    required: true,
                    default: "joerayg blog"
                },
                time_zone: {
                    message: "What time zone will you be publishing under?",
                    required: false,
                    default: "UTC"
                },
                css_hosted_files: {
                    message: "CSS file URLs seperated by commas. 'https://...css,styles/maincss'",
                    required: false,
                    //TODO: remove
                    default: "https://joerayg.me/styles/main.e4396564.css, https://joerayg.me/styles/main.css"
                },
                js_hosted_files: {
                    message: "javascript file URLs seperated by commas. 'https://...js,https://...js'",
                    required: false,
                    //TODO: remove
                    default: "https://joerayg.me/js/main.js, https://joerayg.me/js/supl.js"
                },
                css_local_files: {
                    message: "CSS file URLs seperated by commas. '~/nobjs/main.css, ~/nobjs/supl.css'",
                    required: false,
                    //TODO: remove
                    default: "~/nobjs/main.css, ~/nobjs/supl.css"
                },
                js_local_files: {
                    message: "java script file URLs seperated by commas. '~/nobjs/main.js, ~/nobjs/supl.js'",
                    required: false,
                    //TODO: remove
                    default: "~/nobjs/main.js, ~/nobjs/supl.js"
                },
                // password: {
                //     hidden: true,
                //     replace: '*',
                //     default: "password"
                // }
            }
        };


    // 
    // Start the prompt 
    // 
    prompt.start();



    prompt.get(schema, function(err, result) {
        if (err) {
            throw new Error("getting user input failed", err);
        }


        nobjsBlogConfig.blog_name = result.blog_name;
        nobjsBlogConfig.time_zone = result.time_zone;
        nobjsBlogConfig.css_hosted_files = result.css_hosted_files;
        nobjsBlogConfig.js_hosted_files = result.js_hosted_files;
        nobjsBlogConfig.css_local_files = result.css_local_files;
        nobjsBlogConfig.js_local_files = result.js_local_files;
        nobjsBlogConfig.dirs = {};
        nobjsBlogConfig.dirs.work_directory = result.work_directory;
        nobjsBlogConfig.dirs.publish_directory = result.publish_directory;
        nobjsBlogConfig.dirs.config_directory = util.format("%s/%s", result.work_directory, "config");


        console.log(nobjsBlogConfig);

        mkdirs(nobjsBlogConfig, function(err) {
            if (err) {
                console.error(err);
            }
        });

        var configFile = util.format("%s/%s", nobjsBlogConfig.dirs.config_directory, nobjsBlogConfigFile);

        fs.writeFile(configFile, JSON.stringify(nobjsBlogConfig), function(err) {
            if (err) {
                console.error(
                    err);
            }
        });


    });
}


module.exports = {
    init: init,

    /* test-code */
    __test__mkLocalCfg: mkLocalCfg,
    __test__mkdirs: mkdirs
    /* end-test-code */

};