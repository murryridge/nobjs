var assert = require('chai').assert,
    should = require('chai').should(),
    app = require("../lib/init"),
    admin = require("../lib/admin"),
    fs = require("fs"),
    os = require("os");

describe('Init Module', function() {
    describe('testing mkLocalCfg with no .config', function() {
        after(function() {
            fs.unlinkSync(admin.getConfigLocation(true));
            fs.rmdirSync(os.homedir() + "/.config/nobjs");
            fs.rmdirSync(os.homedir() + "/.config");

        });
        it('should make the global nobjs config path and config file', function(done) {
            app.__test__mkLocalCfg(function(err) {
                if (err) {
                    console.log(err);
                }
                assert.equal(err, null);
                assert.deepEqual(admin.getConfig().blogs, {});
                done();
            });
        });
    });
    //
    describe('testing mkLocalCfg with .config already there', function() {
        before(function() {
            fs.mkdirSync(os.homedir() + "/.config");
        });
        after(function() {
            fs.unlinkSync(admin.getConfigLocation(true));
            fs.rmdirSync(os.homedir() + "/.config/nobjs");
            fs.rmdirSync(os.homedir() + "/.config");

        });
        it('should make the global nobjs config path and config file', function(done) {
            app.__test__mkLocalCfg(function(err) {
                if (err) {
                    console.log(err);
                }
                assert.equal(err, null);
                assert.deepEqual(admin.getConfig().blogs, {});
                done();
            });
        });
    });
    //
    describe('testing mkdirs)', function() {
        it('should return -1 when the value is not present', function() {
            assert.equal(-1, [1, 2, 3].indexOf(5));
            assert.equal(-1, [1, 2, 3].indexOf(0));
        });
    });


    // describe('#indexOf()', function() {
    //     it('should return -1 when the value is not present', function() {
    //         assert.equal(-1, [1, 2, 3].indexOf(5));
    //         assert.equal(-1, [1, 2, 3].indexOf(0));
    //     });
    // });
});