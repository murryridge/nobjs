var assert = require('chai').assert,
    should = require('chai').should(),
    app = require("../lib/admin"),
    fs = require("fs"),
    os = require("os");

describe('Admin', function() {
    describe("test doesn't have home config", function() {
        it('should not have config dirs', function() {
            assert.equal(app.hasHomeConfigDir(), false);
            assert.equal(app.hasNobjsConfigDir(), false);
        });
    });
    //
    describe("test does have home config", function() {
        before(function() {
            fs.mkdirSync(os.homedir() + "/.config");
        });
        after(function() {
            fs.rmdirSync(os.homedir() + "/.config");
        });
        it("should return true", function() {
            assert.equal(app.hasHomeConfigDir(), true);
        });
    });
    //
    describe("test does have home nobjs config dir", function() {
        before(function() {
            fs.mkdirSync(os.homedir() + "/.config");
            fs.mkdirSync(os.homedir() + "/.config/nobjs");
        });
        after(function() {
            fs.rmdirSync(os.homedir() + "/.config/nobjs");
            fs.rmdirSync(os.homedir() + "/.config");
        });
        it("should return true", function() {
            assert.equal(app.hasNobjsConfigDir(), true);
        });
    });
    //
    describe("can read config file", function() {
        before(function() {
            var nobjsConfig = {
                blogs: { testBlog : "TESTBLOG"}
            };
            fs.mkdirSync(os.homedir() + "/.config");
            fs.mkdirSync(os.homedir() + "/.config/nobjs");
            fs.writeFileSync(app.getConfigLocation(true), JSON.stringify(nobjsConfig));
        });
        after(function() {
            fs.unlinkSync(app.getConfigLocation(true));
            fs.rmdirSync(os.homedir() + "/.config/nobjs");
            fs.rmdirSync(os.homedir() + "/.config");
            
        });
        it("should return config obj", function() {
            var config = app.getConfig();
            assert.equal(config.blogs.testBlog, "TESTBLOG");
        });
    });
    //
    describe("test gets config location with and without file", function() {
        it("should return folder and then file path", function() {
            assert.equal(app.getConfigLocation(false), os.homedir() + "/.config/nobjs");
            assert.equal(app.getConfigLocation(true), os.homedir() + "/.config/nobjs/nobjs_config.json");
        });
    });
});